#ifndef _TMATRIX_H_
#define _TMATRIX_H_

#include <iostream>
#include <vector>


namespace tmatrix

{
	class TMatrix{
		protected:
			std::vector<float> v;
			int size_x;
			int size_z;
			float grid_x_step;
			float grid_z_step;
			float x_min, x_max;
			float z_min, z_max;   
		public:
			TMatrix(int size_x, int size_z, float _x_step, float _z_step, float _x_min, float _x_max, float _z_min, float _z_max);
			void addValue(int _pos_x, int _pos_z, float _value);
			float getValue(int _pos_x, int _pos_z);
			int getPosition(float x, float z, int* i, int* j);
			int getSize();
			void printMatrix();
			float getXStep();
			float getZStep();
			int getXSize();
			int getZSize();
	};
}

#endif