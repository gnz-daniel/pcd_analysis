#include <iostream>
#include <string>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <stdlib.h>
#include "tmatrix.h"
#include "obsproj.h"
#include "mathpcl.h"
#include "parameters.h"

pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudMaster (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudMaster_t (new pcl::PointCloud<pcl::PointXYZRGB>);
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
bool update=0;
boost::mutex updateMtx;

int getFileParameters(std::string command, float* x, float* z, float* delta)
{
    std::string name = command.substr(0,command.find('_'));
    cout << command << endl;
    command.erase(0,command.find('_')+1);
    
    std::string x_pos = (command.substr(0,command.find('_')));
    *x = strtof(x_pos.c_str(),NULL);
    command.erase(0,command.find('_')+1);
    
    std::string z_pos = (command.substr(0,command.find('_')));
    *z = strtof(z_pos.c_str(),NULL);
    command.erase(0,command.find('_')+1);
    
    std::string delta_pos = (command.substr(0,command.find('_')));
    *delta = strtof(delta_pos.c_str(),NULL);
    command.erase(0,command.find('_')+1);
}

pcl::PointCloud<pcl::PointXYZRGB>:: Ptr getPointCloudGlobalRef()
{
    float x,z,delta;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
    char pclName[20];
    cin >> pclName;
    if(pcl::io::loadPCDFile<pcl::PointXYZRGB> (pclName, *cloud)==-1)
    {
        cerr << "ERROR: Couldn't read file " << pclName << endl;
        //return 1;
    }
    rotatePointCloudRoll(cloud, CAMERA_ROLL_ANGLE);
    rotatePointCloudPitch(cloud, CAMERA_PITCH_ANGLE);
    std::string command = pclName;
    //getFileParameters(command,&x,&z,&delta);
    cin >> x;
    cout << " x:" << x;
    cin >> z;
    cout << " z:" << z;
    cin >> delta;
    cout << " delta:" << z <<endl;
    rotatePointCloudYaw(cloud, delta);
    shiftPointCloud(cloud,x,0,z);
    return cloud;

}

void addNewPosition()
{
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_t;
    char ss[15];
    while(!viewer->wasStopped ()){
        
        cloud = getPointCloudGlobalRef();

        // Apply the obstacle projection algorithm
        cloud_t = applyTransformation(cloud, 2.5, -0.5);
        boost::mutex::scoped_lock updateLock(updateMtx);

        *cloudMaster = *cloudMaster + *cloud;
        *cloudMaster_t = *cloudMaster_t + *cloud_t;

        // Create the traversable matrix
        //tmatrix::TMatrix *a = createGrid(cloud, GRID_X, GRID_Z);
        update = true;
        updateLock.unlock();
        boost::this_thread::sleep (boost::posix_time::seconds (2));
    }

}


int main(int argc, char** argv)
{
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
    cloudMaster = getPointCloudGlobalRef();
    cloudMaster_t = applyTransformation(cloudMaster, 2.5, -0.5);

    tmatrix::TMatrix *a = createGrid(cloudMaster_t, GRID_X, GRID_Z);

    viewer = showTwoPCL(cloudMaster, cloudMaster_t, *a);

    boost::thread updateThread(addNewPosition);
    while(!viewer->wasStopped ()){
        viewer->spinOnce(100);
        boost::mutex::scoped_lock updateLock(updateMtx);
        if(update)
        {
            updateViewer(viewer, cloudMaster, cloudMaster_t, *a);
            update = false;
        }
        updateLock.unlock();
        boost::this_thread::sleep (boost::posix_time::microseconds (1000000));
    }
    updateThread.join();

    return 0;

}
