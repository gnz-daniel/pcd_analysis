#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

#define CAMERA_X_POS        0
#define CAMERA_Y_POS        0
#define CAMERA_Z_POS        0
#define CAMERA_ROLL_ANGLE   180
#define CAMERA_PITCH_ANGLE  10
#define CAMERA_YAW_ANGLE    0
#define GRID_X				0.5
#define GRID_Z				0.5


#endif