
#include <pcl/common/common.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <stdlib.h>
#include <vector>
#include "mathpcl.h"

void shiftPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, float x, float y, float z)
{
	for(int i=0; i< cloud->size(); i++)
	{
		cloud->points[i].x +=x;
		cloud->points[i].y +=y;
		cloud->points[i].z +=z;
	}
}

void rotatePointCloudYaw(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, float delta)
{
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
    transform_2.rotate (Eigen::AngleAxisf (delta*3.1415/180, Eigen::Vector3f::UnitY()));
    pcl::transformPointCloud(*cloud, *cloud, transform_2);
}
void rotatePointCloudPitch(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, float delta)
{
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
    transform_2.rotate (Eigen::AngleAxisf (delta*3.1415/180, Eigen::Vector3f::UnitX()));
    pcl::transformPointCloud(*cloud, *cloud, transform_2);
}
void rotatePointCloudRoll(pcl::PointCloud<pcl::PointXYZRGB>:: Ptr cloud, float delta)
{
    Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
    transform_2.rotate (Eigen::AngleAxisf (delta*3.1415/180, Eigen::Vector3f::UnitZ()));
    pcl::transformPointCloud(*cloud, *cloud, transform_2);
}
