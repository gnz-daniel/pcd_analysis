#ifndef _MATHPCL_H_
#define _MATHPCL_H_

#include <pcl/common/common.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <stdlib.h>
#include <vector>


void shiftPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, float x, float y, float z);
void rotatePointCloudYaw(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, float delta);
void rotatePointCloudPitch(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, float delta);
void rotatePointCloudRoll(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, float delta);

#endif