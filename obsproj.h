
#include <pcl/visualization/cloud_viewer.h>
#include "tmatrix.h"



boost::shared_ptr<pcl::visualization::PCLVisualizer> showPCL(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud);

boost::shared_ptr<pcl::visualization::PCLVisualizer> showTwoPCL(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud1, pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud2, tmatrix::TMatrix matrix);

pcl::PointCloud<pcl::PointXYZRGB>:: Ptr applyTransformation(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, float height, float ground);

tmatrix::TMatrix* createGrid(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, float grid_x_step, float grid_z_step);

void updateViewer(boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud1, pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud2, tmatrix::TMatrix matrix);
