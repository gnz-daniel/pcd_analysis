#include <iostream>
#include <string>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <stdlib.h>
#include <vector>
#include "obsproj.h"

#define VIEWPORT1 1
#define VIEWPORT2 2
#define VIEWPORT3 3

int cont = 0;

boost::shared_ptr<pcl::visualization::PCLVisualizer> showPCL(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud)
{
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
    viewer->setBackgroundColor (0.3, 0.3, 0.3);

    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
    viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();
    return viewer;
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> showTwoPCL(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud1, pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud2, tmatrix::TMatrix matrix)
{
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->initCameraParameters ();
    int v1 = VIEWPORT1;
    viewer->createViewPort(0.0, 0.0, 0.3, 1.0, v1);
    viewer->setBackgroundColor (1, 1, 1, v1);
    viewer->addText("Original point cloud", 10, 10, "v1 text", v1);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud1);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud1, rgb, "sample cloud1", v1);

    int v2 = VIEWPORT2;
    viewer->createViewPort(0.3, 0.0, 0.6, 1.0, v2);

    
    viewer->setBackgroundColor (1, 1, 1, v2);
    viewer->addText("Transformed point cloud", 10, 10, "v2 text", v2);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb2(cloud2);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud2, rgb2, "sample cloud2", v2);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud1");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud2");
    
    int vg = VIEWPORT3;

    pcl::PointXYZRGB max_point = cloud2->points[0];
    pcl::PointXYZRGB min_point = cloud2->points[0];
    //pcl::getMinMax3D(*cloud2, min_point, max_point);
    for (int i=0; i< cloud2->size(); i++)
    {
        if(min_point.x > cloud2->points[i].x)
            min_point.x = cloud2->points[i].x;
        if(min_point.z > cloud2->points[i].z)
            min_point.z = cloud2->points[i].z;
        if(max_point.x < cloud2->points[i].x)
            max_point.x = cloud2->points[i].x;
        if(max_point.z < cloud2->points[i].z)
            max_point.z = cloud2->points[i].z;
    }
  //  cout << "X: Max and min found, max: " << max_point.x << " min: " << min_point.x << endl;
  //  cout << "Z: Max and min found, max: " << max_point.z << " min: " << min_point.z << endl;

    float grid_x_step = matrix.getXStep();
    float grid_z_step = matrix.getZStep();
    float x_min=0;
    float x_max=0;
    float z_min=0;
    float z_max=0;
    double r,g,b;

    //viewer->addSphere(basic_point, 1, "sphere", v2);
    viewer->createViewPort(0.6 ,0.0 ,0.9 ,1.0, vg);
    for(int i = 0; i< matrix.getXSize() ; i++){
        for(int j=0; j<matrix.getZSize(); j++)
        {   //char str[10]; 
            char str[20] ;
            sprintf(str, "CubeX%dY%d", i,j);
            if(matrix.getValue(i,j) == 1)
            {
                r = 1.0;
                g = 0;
                b = 0;
            }
            else if(matrix.getValue(i,j) == 0)
            {
                r = 0;
                g = 1.0;
                b = 0;
            }
            else
            {
                r = 0.1;
                g = 0.1;
                b = 0.1;
            }
            x_min= min_point.x + (2*i-1)*grid_x_step/2;
            x_max= min_point.x + (2*i+1)*grid_x_step/2;
            z_min= min_point.z + (2*j-1)*grid_z_step/2;
            z_max= min_point.z + (2*j+1)*grid_z_step/2;
            viewer->addCube(x_min,x_max, -1.1, -1.0,z_min,z_max,r,g,b,str,vg);
        }
        viewer->addCoordinateSystem (1.0);

    }
    char text[30];
    sprintf(text,"Grid , x step:%.2f  z step:%.2f", grid_x_step, grid_z_step);
    viewer->setBackgroundColor(1,1,1,vg);
    viewer->addText(text, 10, 10, "vg text", vg);
    //viewer->addCube(-1,1,-1,1,-1,1,1,1,1,,vg);
    // addCube(Xmin,Xmax,Ymin,Ymax,Zmin,Zmax, R, G, B, ID, VP) -> RGB = (0:1) , press S at viewer

  return (viewer);
}

void updateViewer(boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer, pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud1, pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud2, tmatrix::TMatrix matrix)
{
    char str1[20], str2[20];
    sprintf(str1, "update1_%d",cont);
    sprintf(str2, "update2_%d",cont);
    cont++;
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb1(cloud1);
    viewer->addPointCloud(cloud1, rgb1, str1 , VIEWPORT1);

    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb2(cloud2);
    viewer->addPointCloud(cloud2, rgb2, str2 , VIEWPORT2);

}

pcl::PointCloud<pcl::PointXYZRGB>:: Ptr applyTransformation(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, float height, float ground)
{
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_s (new pcl::PointCloud<pcl::PointXYZRGB>);
    //copyPointCloud(*cloud,*cloud_s);

    //cloud_s->clear();
    int cloud_size = 0;
 
    for(int i= 0; i< cloud->height * cloud->width; i++)
    {

        pcl::PointXYZRGB basic_point;
        basic_point.x = cloud->points[i].x;
        basic_point.y = cloud->points[i].y;
        basic_point.z = cloud->points[i].z;
        if(basic_point.y < height && basic_point.y > ground)
        {

            basic_point.r = (uint8_t)255;
            basic_point.g = (uint8_t)10;
            basic_point.b = (uint8_t)10;
            cloud_s->points.push_back (basic_point);
            cloud_size++;
        }
        else if( basic_point.y < ground )
        {

            basic_point.r = (uint8_t)10;
            basic_point.g = (uint8_t)255;
            basic_point.b = (uint8_t)10;
            cloud_s->points.push_back (basic_point);
            cloud_size++; 
        }
    }
    cloud_s->width = cloud_size;
    cout << "Cloud _s size: " << cloud_s->size() << endl;

    return (cloud_s);
}

tmatrix::TMatrix* createGrid(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, float grid_x_step, float grid_z_step)
{
    pcl::PointXYZRGB max_point = cloud->points[20];
    pcl::PointXYZRGB min_point = cloud->points[20];
    //pcl::getMinMax3D(*cloud, min_point, max_point);
    for (int i=0; i< cloud->size(); i++)
    {
        if(min_point.x > cloud->points[i].x)
            min_point.x = cloud->points[i].x;
        if(min_point.z > cloud->points[i].z)
            min_point.z = cloud->points[i].z;
        if(max_point.x < cloud->points[i].x)
            max_point.x = cloud->points[i].x;
        if(max_point.z < cloud->points[i].z)
            max_point.z = cloud->points[i].z;
    }
    
   // cout << "X: Max and min found, max: " << max_point.x << " min: " << min_point.x << endl;
   // cout << "Z: Max and min found, max: " << max_point.z << " min: " << min_point.z << endl;
    //int matrix[20][20];
    int size_x = (int)(1 +(float)((max_point.x - min_point.x - grid_x_step)/grid_x_step));
    int size_z = (int)(1 +(float)((max_point.z - min_point.z - grid_z_step)/grid_z_step));
    tmatrix::TMatrix *tmatrix = new tmatrix::TMatrix(size_x, size_z, grid_x_step, grid_z_step, min_point.x, max_point.x, min_point.z, max_point.z);
    for(int x =0; x<= size_x ; x++)
    {
        for(int z = 0; z<= size_z; z++)
        {
            for(int i = 0; i< cloud->size() ; i++)
            {
                float x_min= min_point.x + (2*x-1)*grid_x_step/2;
                float x_max= min_point.x + (2*x+1)*grid_x_step/2;
                float z_min= min_point.z + (2*z-1)*grid_z_step/2;
                float z_max= min_point.z + (2*z+1)*grid_z_step/2;

                if(cloud->points[i].r == 255)
                {
                    if(cloud->points[i].x > x_min && cloud->points[i].x < x_max)
                    {
                        if(cloud->points[i].z > z_min && cloud->points[i].z < z_max)
                        {
                            tmatrix->addValue(x,z,1);
                            break;
                        }
                    }
                }
                else if( cloud->points[i].g == 255 && tmatrix->getValue(x,z) != 1)
                {
                    if(cloud->points[i].x > x_min && cloud->points[i].x < x_max)
                    {
                        if(cloud->points[i].z > z_min && cloud->points[i].z < z_max)
                        {
                            tmatrix->addValue(x,z,0);
                          //  break;
                        }
                    }
                }
                else 
                {
                    //tmatrix->addValue(x,z,-1);
                }
            }
        }
    }
    return(tmatrix);
}