#include "tmatrix.h"
#include <vector>
#include <iostream>

using namespace std;

namespace tmatrix
{

    TMatrix::TMatrix(int _size_x, int _size_z, float _x_step, float _z_step, float _x_min, float _x_max, float _z_min, float _z_max)
    {
        size_x = _size_x;
        size_z = _size_z;
        x_min = _x_min;
        x_max = _x_max;
        z_min = _z_min;
        z_max = _z_max;
        grid_x_step = _x_step;
        grid_z_step = _z_step;
        v.resize(size_x*size_z, 2);
    }

    void TMatrix::addValue(int _pos_x, int _pos_z, float _value)
    {
        if(_pos_z*size_x + _pos_x < size_x*size_z )
        {
            //cout << " X: "<< _pos_x << "  Z:" << _pos_z << "  V:" << _value << endl;
            v[_pos_z*size_x + _pos_x ] = _value ;
            //cout << " " << v[_pos_x*size_z + _pos_z ] << endl;
        }
    }

    float TMatrix::getValue(int _pos_x, int _pos_z)
    {
          if(_pos_x*_pos_z < v.size())
        {
            return v[_pos_z*size_x + _pos_x];
        }else
            return -1;  
    }

    int TMatrix::getPosition(float x, float z, int* i, int* j)
    {
        if( x < this->x_min || x > this->x_max)
            return 1;
        if( z < this->z_min || z > this->z_max)
            return 1;

        for(int m =0; m <= size_x ; m++)
        {
            for(int n = 0; n <= size_z; n++)
            {
                float x_inf= this->x_min + (2*m-1)*grid_x_step/2;
                float x_sup= this->x_min + (2*m+1)*grid_x_step/2;
                float z_inf= this->z_min + (2*n-1)*grid_z_step/2;
                float z_sup= this->z_min + (2*n+1)*grid_z_step/2;

                if(x >= x_inf && x <= x_sup)
                {
                    if(z >= z_inf && z <= z_sup)
                    {
                        *i = m;
                        *j = n;
                        return 0;
                    }
                }
            }
        }
    }

    int TMatrix::getSize()
    {
        return v.size();
    }

    float TMatrix::getXStep()
    {
        return grid_x_step;
    }

    float TMatrix::getZStep()
    {
        return grid_z_step;
    }

    int TMatrix::getXSize()
    {
        return size_x;
    }

    int TMatrix::getZSize()
    {
        return size_z;
    }
    void TMatrix::printMatrix()
    {
        cout << "Max X: " << this->x_max << " Min X: " << this-> x_min << endl;
        cout << "Max Z: " << this->z_max << " Min Z: " << this-> z_min << endl;
        for(int i = 0; i < size_z; i++)
        {
            for(int j = 0; j< size_x; j++)
            {
                cout << v[i*size_x + j] << " ";
            }
            cout << endl;
        }
    }
}